server {

	listen   80;

	server_name  ducky-pond.com;

	access_log  /var/log/nginx/ducky-pond.com.access.log;


	
	location / {
	
		root   /var/www;
	
		index  index.html;
	
	}
	


}
