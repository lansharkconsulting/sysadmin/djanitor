from django.http import HttpResponse
from process.models import ProcessManager

class StartupChecks(object):
	def process_request(self, request):
		n = ProcessManager("nginx")
		if n.exists(["-v"]):
			return None
		else:
			return HttpResponse("Please install Nginx")
