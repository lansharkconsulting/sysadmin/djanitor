from subprocess import Popen, PIPE, STDOUT

def run_process(args, fds=True):
	try:
		p = Popen(args, stdout=PIPE, stdin=PIPE, stderr=STDOUT, close_fds=fds)
		o = p.stdout.readlines()
	except OSError:
		return None

	if o:
		return o
	else:
		return None

class ProcessManager(object):
	def __init__(self, name):
		self.name = name

	def running(self):
		return run_process(["pgrep", self.name])

	def exists(self, args):
		return run_process([self.name] + args)

	def run(self, args):
		return run_process(args)


class SystemMonitor(object):
	def __init__(self, ram_args):
		self.ram_args = ram_args

	def ram_usage():
		p = Popen(ram_args, stdout=PIPE, stdin=PIPE, stderr=STDOUT)
		o = p.stdout.readlines()
		return o

