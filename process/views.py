from django.shortcuts import render, redirect
from models import ProcessManager
from settings import NGINX_BIN, NGINX_PREFIX, NGINX_CONF

def nginx_index(request):
	n = ProcessManager(NGINX_BIN)
	info = n.exists(["-v"])
	proc = n.running()
	return render(request, "process/nginx_index.html", { "info":info, "proc":proc })

def nginx_start(request):
	n = ProcessManager(NGINX_BIN)
	log = n.run([NGINX_BIN,"-c",NGINX_CONF,"-p",NGINX_PREFIX])
	return render(request, "process/nginx_start.html", { "log":log })

def nginx_stop(request):
	n = ProcessManager(NGINX_BIN)
	log = n.run([NGINX_BIN,"-s","stop"])
	return render(request, "process/nginx_stop.html", { "log":log })
