from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:B
    url(r'^$', 'djanitor.monitor.views.index'),
    url(r'^process/nginx_index/$', 'djanitor.process.views.nginx_index'),
    url(r'^process/nginx_start/$', 'djanitor.process.views.nginx_start'),
    url(r'^process/nginx_stop/$', 'djanitor.process.views.nginx_stop'),

    url(r'^vhost/$', 'djanitor.vhost.views.index'),
    url(r'^vhost/toggle/$', 'djanitor.vhost.views.toggle'),
    url(r'^vhost/add/$', 'djanitor.vhost.views.add'),
    url(r'^vhost/save/$', 'djanitor.vhost.views.save'),
    # url(r'^djanitor/', include('djanitor.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
